# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/first_contribution_merged_message'

RSpec.describe Triage::FirstContributionMergedMessage do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        object_kind: 'merge_request'
      }
    end
  end

  let(:first_contribution) { true }

  before do
    allow(event).to receive(:first_contribution?).and_return(first_contribution)
  end

  include_context 'with merge request notes'

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.merge']

  describe '#applicable?' do
    it_behaves_like 'community contribution processor #applicable?'

    context 'when there is already a comment for the same purpose' do
      let(:merge_request_notes) do
        [
          { body: 'review comment 1' },
          { body: comment_mark }
        ]
      end

      include_examples 'event is not applicable'
    end

    context 'when the merge request is not a first contribution' do
      let(:first_contribution) { false }

      include_examples 'event is not applicable'
    end

    context 'when the merge request is labelled as spam' do
      let(:label_names) { ['Spam'] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts code review experience message' do
      body = add_automation_suffix('processor/community/first_contribution_merged_message.rb') do
        <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          @#{event.resource_author.username}, congratulations for getting your first MR merged :tada:

          If this is your first MR against a GitLab project, we'd like to invite and encourage you to self-nominate yourself for `First MR Merged` swag prize [here](https://docs.google.com/forms/d/e/1FAIpQLSfGo-3kEimVPpC5zKKxXHkFjgYx8-vQAanzAX2LxGgXQqXikQ/viewform).

          Thank you again for contributing, what's your next contribution going to be? :thinking:
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
