# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/rack/processor'

RSpec.describe Triage::Rack::Processor do
  include_context 'with event'

  let(:app)      { described_class.new }
  let(:env)      { { payload: payload } }
  let(:response) { app.call(env) }
  let(:status)   { response[0] }
  let(:body)     { response[2][0] }

  before do
    allow(Triage::ProcessorJob).to receive(:perform_async).with(payload)
  end

  it 'pass the event to the ProcessorJob worker' do
    expect(Triage::ProcessorJob).to receive(:perform_async).with(payload)

    expect(response).to be_an(Array)
  end

  it 'returns a 200 response' do
    expect(body).to eq(JSON.dump(status: :ok))
    expect(status).to eq(200)
  end
end
