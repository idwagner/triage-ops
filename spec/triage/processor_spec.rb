# frozen_string_literal: true
require 'spec_helper'

require_relative '../../triage/triage/processor'

RSpec.describe Triage::Processor do
  describe '.react_to_approvals' do
    it 'reacts to merge_request.approval and merge_request.approved' do
      processor = Class.new(described_class)

      expect(processor).to receive(:react_to).with('merge_request.approval', 'merge_request.approved')

      processor.react_to_approvals
    end
  end

  describe '.react_to and .listeners' do
    context "when react to 'issue.open', 'issue.note', 'merge_request.*'" do
      include_examples 'registers listeners', ['issue.note', 'issue.open', 'merge_request.approval', 'merge_request.approved', 'merge_request.close', 'merge_request.merge', 'merge_request.note', 'merge_request.update', 'merge_request.open', 'merge_request.reopen', 'merge_request.unapproval', 'merge_request.unapproved'] do
        let(:processor) do
          Class.new(described_class) do
            react_to 'issue.open', 'issue.note', 'merge_request.*'
          end
        end
      end
    end

    context "when react to '*.*'" do
      include_examples 'registers listeners', ['issue.note', 'issue.open', 'issue.close', 'issue.reopen', 'issue.update', 'incident.note', 'incident.open', 'incident.close', 'incident.reopen', 'incident.update', 'merge_request.approval', 'merge_request.approved', 'merge_request.close', 'merge_request.merge', 'merge_request.note', 'merge_request.update', 'merge_request.open', 'merge_request.reopen', 'merge_request.unapproval', 'merge_request.unapproved', 'monitoring.uptime_check', 'pipeline.canceled', 'pipeline.failed', 'pipeline.success'] do
        let(:processor) do
          Class.new(described_class) do
            react_to '*.*'
          end
        end
      end
    end
  end

  describe '#triage' do
    subject { described_class.new(double(:event)) }

    context 'when event is applicable' do
      it 'calls hook methods' do
        expect(subject).to receive(:applicable?).ordered.and_return(true)
        expect(subject).to receive(:before_process).ordered
        expect(subject).to receive(:process).ordered
        expect(subject).to receive(:after_process).ordered

        subject.triage
      end

      context 'with a subclass' do
        let(:processor) do
          Class.new(described_class) do
            def process
              'a message'
            end
          end
        end

        subject { processor.new(double(:event)) }

        it 'returns the result of #process' do
          expect(subject.triage).to eq('a message')
        end
      end
    end

    context 'when event is not applicable' do
      it 'does not call hooks' do
        expect(subject).to receive(:applicable?).and_return(false)
        expect(subject).not_to receive(:before_process)
        expect(subject).not_to receive(:process)
        expect(subject).not_to receive(:after_process)

        subject.triage
      end
    end
  end

  describe '#documentation' do
    subject { described_class.new(double(:event)) }

    it 'is empty' do
      expect(subject.documentation).to be_empty
    end
  end
end
