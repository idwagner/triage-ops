# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/missed_resource_helper'

RSpec.describe MissedResourceHelper do
  let(:milestone) { double(:milestone, due_date: due_date, title: '12.5') }
  let(:due_date) { Date.new(2019, 11, 17) }
  let(:release_date) { Date.new(2019, 11, 22) }

  let(:resource_klass) do
    Struct.new(:issue) do
      include MissedResourceHelper
    end
  end

  subject { resource_klass.new }

  describe '#missed_resource?' do
    it 'returns false if milestone due date is nil' do
      expect(subject.missed_resource?(nil)).to be false
    end

    it 'returns false on prior date of milestone due date' do
      expect(subject.missed_resource?(milestone, due_date, due_date - 1)).to be false
    end

    it 'returns false when date is the milestone due date' do
      expect(subject.missed_resource?(milestone, release_date, due_date)).to be false
    end

    it 'returns true when date is the milestone release date' do
      expect(subject.missed_resource?(milestone, release_date, release_date)).to be true
    end

    it 'returns true on the day after the milestone release date' do
      expect(subject.missed_resource?(milestone, release_date, release_date + 1)).to be true
    end

    context 'when release date is Thursday' do
      let(:release_date) { Date.new(2019, 11, 21) }

      it 'returns false for Tuesday' do
        expect(subject.missed_resource?(milestone, release_date, release_date - 2)).to be false
      end

      it 'returns true for Wednesday' do
        expect(subject.missed_resource?(milestone, release_date, release_date - 1)).to be true
      end
    end

    context 'when release date is Friday' do
      let(:release_date) { Date.new(2019, 11, 22) }

      it 'returns false for Wednesday' do
        expect(subject.missed_resource?(milestone, release_date, release_date - 2)).to be false
      end

      it 'returns true for Thursday' do
        expect(subject.missed_resource?(milestone, release_date, release_date - 1)).to be true
      end
    end

    context 'when release date is Saturday' do
      let(:release_date) { Date.new(2019, 11, 23) }

      it 'returns false for Thursday' do
        expect(subject.missed_resource?(milestone, release_date, release_date - 2)).to be false
      end

      it 'returns true for Friday' do
        expect(subject.missed_resource?(milestone, release_date, release_date - 1)).to be true
      end
    end

    context 'when release date is Sunday' do
      let(:release_date) { Date.new(2019, 11, 24) }

      it 'returns false for Thursday' do
        expect(subject.missed_resource?(milestone, release_date, release_date - 3)).to be false
      end

      it 'returns true for Friday' do
        expect(subject.missed_resource?(milestone, release_date, release_date - 2)).to be true
      end

      it 'returns true for Saturday' do
        expect(subject.missed_resource?(milestone, release_date, release_date - 1)).to be true
      end
    end

    context 'when release date is Monday' do
      let(:release_date) { Date.new(2019, 11, 25) }

      it 'returns false for preceding Thursday' do
        expect(subject.missed_resource?(milestone, release_date, release_date - 4)).to be false
      end

      it 'returns true for preceding Friday' do
        expect(subject.missed_resource?(milestone, release_date, release_date - 3)).to be true
      end

      it 'returns true for preceding Saturday' do
        expect(subject.missed_resource?(milestone, release_date, release_date - 2)).to be true
      end

      it 'returns true for preceding Sunday' do
        expect(subject.missed_resource?(milestone, release_date, release_date - 1)).to be true
      end
    end

    context 'when release date is Tuesday' do
      let(:release_date) { Date.new(2019, 11, 26) }

      it 'returns false for Sunday' do
        expect(subject.missed_resource?(milestone, release_date, release_date - 2)).to be false
      end

      it 'returns true for prior day' do
        expect(subject.missed_resource?(milestone, release_date, release_date - 1)).to be true
      end
    end

    context 'when release date is not provided' do
      # The fixture data contain the actual release date data,
      # which happens to equal to the release date here, because
      # here we're also using the actual release date. This means
      # we can use the release_date here even when we're not passing it,
      # because from the fixture data it should find the same data.
      include_context 'with release date file stubbed'

      before do
        allow(subject).to receive(:today).and_return(today)
      end

      context 'when today is release date' do
        let(:today) { release_date }

        it 'returns true' do
          expect(subject.missed_resource?(milestone)).to be(true)
        end
      end

      context 'when today is 1 day before release date' do
        let(:today) { release_date - 1 }

        it 'returns true' do
          expect(subject.missed_resource?(milestone)).to be(true)
        end
      end

      context 'when today is 2 days before release date' do
        let(:today) { release_date - 2 }

        it 'returns false' do
          expect(subject.missed_resource?(milestone)).to be(false)
        end
      end
    end
  end

  describe '#add_missed_labels' do
    context 'when there is no Deliverable label' do
      it 'returns /label ~"missed:x.y"' do
        actions = subject.add_missed_labels('12.5', [])

        expect(actions).to eq('/label ~"missed:12.5"')
      end
    end

    context 'when there is a Deliverable label' do
      it 'returns /label ~"missed:x.y"' do
        actions = subject.add_missed_labels('12.5', %w[Deliverable])

        expect(actions).to eq(<<~ACTIONS.chomp)
          /label ~"missed:12.5"
          /label ~"missed-deliverable"
        ACTIONS
      end
    end
  end

  describe '#move_to_current_milestone' do
    context 'when the current milestone is 12.6' do
      let!(:versioned_milestone) { VersionedMilestone.new(subject) }

      before do
        allow(VersionedMilestone)
          .to receive(:new)
          .with(subject)
          .and_return(versioned_milestone)

        allow(versioned_milestone)
          .to receive_message_chain(:current, :title)
          .and_return('12.6')
      end

      it 'returns a quick command to set to 12.6' do
        actions = subject.move_to_current_milestone

        expect(actions).to eq('/milestone %"12.6"')
      end
    end
  end
end
