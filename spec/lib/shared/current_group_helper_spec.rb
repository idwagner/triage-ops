# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/devops_labels'
require_relative '../../../lib/shared/current_group_helper'
require_relative '../../../lib/shared/resource_group'
require_relative '../../../lib/www_gitlab_com'

RSpec.describe CurrentGroupHelper do
  let(:resource_klass) do
    Struct.new(:current_group_name, :label_names) do
      include CurrentGroupHelper
    end
  end

  let(:label_names)           { ['group::triage ops', 'frontend'] }
  let(:current_group_name)    { 'source_code' }
  let(:group_name_with_space) { 'source code' }
  let(:current_group)         { ResourceGroup.new(current_group_name, label_names) }
  let(:expected_frontend_em)  { '@andr3' }
  let(:expected_backend_em)   { '@sean_carroll' }
  let(:team_data)             { {} }

  subject { resource_klass.new(current_group_name, label_names) }

  before do
    allow(ResourceGroup).to receive(:new).with(current_group_name, label_names).and_return(current_group)
    allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_data)
  end

  describe '#current_group' do
    it 'returns ResourceGroup object for the current group' do
      expect(subject.current_group).to eq(current_group)
    end
  end

  describe '#current_group_em' do
    context 'with frontend label' do
      it 'returns gitlab handle for frontend engineering manager' do
        expect(subject.current_group_em).to eq(expected_frontend_em)
      end
    end

    context 'without frontend label' do
      let(:label_names) { ['source_code'] }

      it 'returns gitlab handle for backend engineering manager' do
        expect(subject.current_group_em).to eq(expected_backend_em)
      end
    end
  end

  describe '#current_group_frontend_em' do
    it 'returns gitlab handle for frontend engineering manager' do
      expect(subject.current_group_frontend_em).to eq(expected_frontend_em)
    end
  end

  describe '#current_group_backend_em' do
    it 'returns gitlab handle for backend engineering manager' do
      expect(subject.current_group_backend_em).to eq(expected_backend_em)
    end
  end

  describe '#current_group_set' do
    let(:set) do
      { "role" => "Senior Software Engineer in Test",
        "departments" => ["Engineering Function", "Quality Department"] }
    end

    context 'when SETs populated their team info in the specialty field' do
      let(:team_data) do
        { "set1" => set.merge({ "specialty" => group_name_with_space }),
          "set2" => set.merge({ "specialty" => group_name_with_space }) }
      end

      it 'returns gitlab handle for the first software engineer in test' do
        expect(subject.current_group_set).to eq('@set1')
      end
    end

    context "when SETs populated team info in the role field" do
      let(:team_data) do
        { "set1" => set.merge({ "specialty" => nil, "role" => "Senior Software Engineer in Test, #{group_name_with_space}" }),
          "set2" => set.merge({ "specialty" => nil, "role" => "Senior Software Engineer in Test, #{group_name_with_space}" }) }
      end

      it "returns the username of the first SET" do
        expect(subject.current_group_set).to eq("@set1")
      end
    end

    context "when multiple SETs found, some populated group info in specialty field, some in the role field" do
      let(:team_data) do
        { "set1" => set.merge({ "specialty" => nil, "role" => "Senior Software Engineer in Test, #{group_name_with_space}" }),
          "set2" => set.merge({ "specialty" => group_name_with_space }) }
      end

      it "returns the username of the SET who specified team info in specialty" do
        expect(subject.current_group_set).to eq("@set2")
      end
    end

    context "when SET is not found for the current group" do
      let(:team_data) do
        { "set0" => set, "set1" => set }
      end

      it 'returns nil' do
        expect(subject.current_group_set).to be_nil
      end
    end
  end

  describe '#current_group_pm' do
    let(:expected_pm) { '@derekferguson' }

    it 'returns the current group product manager' do
      expect(subject.current_group_pm).to eq(expected_pm)
    end
  end

  describe '#current_group_pd' do
    let(:designer) do
      { "role" => "Product Designer",
        "departments" => ["Engineering Function", "UX Department"] }
    end

    before do
      allow(WwwGitLabCom).to receive(:team_from_www).and_return(team_data)
    end

    context "when product designers populated team info in the specialty field" do
      let(:team_data) do
        { "pd1" => designer.merge({ "specialty" => group_name_with_space }) }
      end

      it "returns the username of the first product designer" do
        expect(subject.current_group_pd).to eq("@pd1")
      end
    end

    context "when product designers populated team info in the role field" do
      let(:team_data) do
        { "pd1" => designer.merge({ "specialty" => nil, "role" => "Product Designer, #{group_name_with_space}" }),
          "pd2" => designer.merge({ "specialty" => nil, "role" => "Product Designer, #{group_name_with_space}" }) }
      end

      it "returns the username of the first product designer" do
        expect(subject.current_group_pd).to eq("@pd1")
      end
    end

    context "when some product designers populated team info in the role field, some in special field" do
      let(:team_data) do
        { "pd1" => designer.merge({ "specialty" => nil, "role" => "Product Designer, #{group_name_with_space}" }),
          "pd2" => designer.merge({ "specialty" => group_name_with_space }) }
      end

      it "returns the username of product designer who specified team info in specialty" do
        expect(subject.current_group_pd).to eq("@pd2")
      end
    end

    context "when product designer is not found for the current group" do
      let(:team_data) do
        { "pd1" => designer, "pd2" => designer }
      end

      it 'returns nil' do
        expect(subject.current_group_pd).to be_nil
      end
    end
  end
end
