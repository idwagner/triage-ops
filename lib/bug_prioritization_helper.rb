# frozen_string_literal: true

require_relative 'constants/gitlab_org'
require_relative 'group_triage_helper'

module BugPrioritizationHelperContext
  BUG_REPORT_DUE = 'in 6 weeks'
  BUG_REPORT_LABELS = ['triage report', 'type::ignore'].freeze

  include GroupTriageHelperContext

  def bug_prioritization_summary(items:, assignees: [], labels: [], mentions: [], **_args)
    <<~SUMMARY
      #{default_greetings(assignees)}

      This is a group level Bug Prioritization report that aims to summarize
      the bugs which have been prioritized for the upcoming milestone %"#{upcoming_milestone_title}"
      based on the Severity, Security vulnerabilities and Customer labels.
      For more information please refer to the handbook:
      - https://about.gitlab.com/handbook/engineering/quality/quality-engineering/bug-prioritization/index.html#bug-prioritization-triage-reports

      - Determine if the issue should be closed if it is no longer relevant or a
      duplicate.
      - Please work with your team to identify the issues that can be worked on
      for the upcoming milestone %"#{upcoming_milestone_title}".
      - For more questions/clarifications, Please reach out to your team's Software
      Engineer in Test or the `#quality` Slack channel for assistance.

      #{items.lines.reject(&:blank?).first(15).join}

      #{in_cc(mentions)}
      /label #{build_labels(labels)}
      /label #{build_labels(BUG_REPORT_LABELS)}
      /due #{BUG_REPORT_DUE}
      /milestone %"#{upcoming_milestone_title}"
      /assign #{build_mentions(assignees)}

      _This report has been set to confidential as it may contain information about the priority and severity of security issues in each group._

      /confidential

    SUMMARY
  end

  def bug_prioritization_title(group_name)
    "#{Date.today.iso8601} - Bugs Prioritization for \"#{group_name}\" for upcoming milestone - #{upcoming_milestone_title}"
  end

  private

  def upcoming_milestone_title
    @active_milestones ||= Triage.api_client.group_milestones(GitlabOrg::GROUP_ID, state: 'active').reject(&:expired)
    @active_milestones[1]&.title
  end
end
