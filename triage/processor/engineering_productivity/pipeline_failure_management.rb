# frozen_string_literal: true

require_relative '../../triage/processor'
require_relative '../../triage/pipeline_failure/pipeline_incident_finder'
require_relative '../../triage/pipeline_failure/config_selector'
require_relative '../../triage/pipeline_failure/config/dev_master_branch'
require_relative '../../triage/pipeline_failure/config/gitaly_update_branch'
require_relative '../../triage/pipeline_failure/config/master_branch'
require_relative '../../triage/pipeline_failure/config/review_app_child_pipeline'
require_relative '../../triage/pipeline_failure/config/ruby2_branch'
require_relative '../../triage/pipeline_failure/config/security_branch'
require_relative '../../triage/pipeline_failure/config/stable_branch'
require_relative '../../triage/pipeline_failure/failed_jobs'
require_relative '../../triage/pipeline_failure/incident_creator'
require_relative '../../triage/pipeline_failure/slack_notifier'

module Triage
  class PipelineFailureManagement < Processor
    CONFIGURATION_CLASSES = [
      Triage::PipelineFailure::Config::DevMasterBranch,
      Triage::PipelineFailure::Config::GitalyUpdateBranch,
      Triage::PipelineFailure::Config::MasterBranch,
      Triage::PipelineFailure::Config::ReviewAppChildPipeline,
      Triage::PipelineFailure::Config::Ruby2Branch,
      Triage::PipelineFailure::Config::SecurityBranch,
      Triage::PipelineFailure::Config::StableBranch
    ].freeze

    CLOSED = 'closed'
    OPENED = 'opened'
    FAILED = 'failed'
    SUCCESS = 'success'

    STATUSES = {
      FAILED => { emoji: ':x:' },
      SUCCESS => { emoji: ':white_check_mark:' }
    }.freeze

    react_to 'pipeline.failed', 'pipeline.success'

    def applicable?
      return false unless config

      pipeline_status != SUCCESS || latest_incident
    end

    def process
      return update_latest_incident if pipeline_status == SUCCESS

      up_to_date_incident = upsert_incident
      send_slack_notification(up_to_date_incident) unless closed?(up_to_date_incident) || previously_notified?
    end

    def documentation
      <<~TEXT
        This processor creates a pipeline failure incident and post it to Slack.
      TEXT
    end

    private

    def config
      @config ||= PipelineFailure::ConfigSelector.find_config(event, CONFIGURATION_CLASSES)
    end

    def pipeline_status
      @pipeline_status ||= event.status
    end

    def failed_jobs
      @failed_jobs ||= Triage::PipelineFailure::FailedJobs.new(event).execute
    end

    def create_incident
      return unless config.create_incident?

      incident_creator.execute
    end

    def pipeline_link
      "[##{event.id}](#{event.web_url})"
    end

    def incident_creator
      @incident_creator ||= Triage::PipelineFailure::IncidentCreator.new(
        event: event,
        config: config,
        failed_jobs: failed_jobs)
    end

    def incident_finder
      @incident_finder ||= Triage::PipelineFailure::PipelineIncidentFinder.new(
        incident_project_id: config.incident_project_id,
        pipeline_id: event.id
      )
    end

    def upsert_incident
      return unless config.create_incident?

      update_latest_incident || create_incident
    end

    def latest_incident
      return unless config.create_incident?

      @latest_incident ||= incident_finder.latest_incident
    end

    def update_latest_incident
      return unless latest_incident

      add_discussion(
        "#{pipeline_link} retry status: #{pipeline_status} #{STATUSES.dig(pipeline_status, :emoji)}.",
        "/projects/#{latest_incident.project_id}/issues/#{latest_incident.iid}",
        append_source_link: false
      )

      return reopen_incident(latest_incident) if reopen?

      latest_incident
    end

    def reopen?
      return false unless latest_incident

      closed?(latest_incident) && pipeline_status == FAILED
    end

    def closed?(incident)
      incident&.state == CLOSED
    end

    def previously_notified?
      # if the latest incident is still open, we know it was already notified and still under investigation
      latest_incident&.state == OPENED
    end

    def reopen_incident(incident)
      Triage.api_client.reopen_issue(incident.project_id, incident.iid)
    end

    def send_slack_notification(incident)
      Triage::PipelineFailure::SlackNotifier.new(
        event: event,
        config: config,
        failed_jobs: failed_jobs,
        slack_webhook_url: ENV.fetch('SLACK_WEBHOOK_URL'),
        incident: incident
      ).execute
    end
  end
end
