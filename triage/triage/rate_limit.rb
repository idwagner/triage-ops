# frozen_string_literal: true

module Triage
  module RateLimit
    # Rate limit triage requests to
    # a maximum of `rate_limit_count` (default: 60)
    # over a `rate_limit_period` (default: 3600 seconds / 1 hour)
    #
    # Instance methods to be implemented:
    # - `#cache_key` [required]: identifies related requests to be rate limited
    # - `#rate_limit_count` [optional]: maximum number of requests allowed in the period
    # - `#rate_limit_period` [optional]: number of seconds until rate limit is reset

    DEFAULT_RATE_LIMIT_COUNT = 60
    DEFAULT_RATE_LIMIT_PERIOD = 3600 # 1 hour

    def self.included(base)
      base.prepend(Hooks)
    end

    private

    def rate_limited?
      Triage.cache.get(cache_key).to_i >= rate_limit_count
    end

    def increment_commands_sent
      new_counter_value, expires_in = cache_counter_data

      Triage.cache.set(cache_key, new_counter_value, expires_in: expires_in)
    end

    def cache_counter_data
      if Triage.cache.set?(cache_key)
        current_counter_data = Triage.cache.data[cache_key]
        remaining_seconds_until_expiry = (current_counter_data.expires_in - Time.now).round

        [current_counter_data.value + 1, remaining_seconds_until_expiry]
      else
        [1, rate_limit_period]
      end
    end

    def cache_key
      raise NotImplementedError
    end

    def rate_limit_count
      DEFAULT_RATE_LIMIT_COUNT
    end

    def rate_limit_period
      DEFAULT_RATE_LIMIT_PERIOD
    end

    module Hooks
      def applicable?
        return false if rate_limited?

        super
      end

      def before_process
        increment_commands_sent

        super
      end
    end
  end
end
